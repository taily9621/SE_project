package com.example.gram.todo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Settings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    public void openSettings_changepw(View view){
        Intent intent = new Intent(this, Settings_changepw.class);
        startActivity(intent);
    }

    public void openSettings_removeid(View view){
        Intent intent = new Intent(this, Settings_removeid.class);
        startActivity(intent);
    }

    public void openSettings_complete(View view){
        Intent intent = new Intent(this, Settings_complete.class);
        startActivity(intent);
    }

    public void openSettings_subject(View view){
        Intent intent = new Intent(this, Settings_subject.class);
        startActivity(intent);
    }
}
